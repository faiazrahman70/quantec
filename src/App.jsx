import { useState } from 'react'
import './App.css'

import { Hero } from './pages'
import AnimatedCursor from 'react-animated-cursor'

function App() {
	return (
		<div className="App">
			<AnimatedCursor
				innerSize={8}
				outerSize={45}
				color="255,255,255"
				outerAlpha={0.2}
				outerStyle={{
					color: 'rgba(255,255,255,0.2)',
				}}
				innerScale={0.7}
				outerScale={2.1}
				clickables={[
					'a',
					'input[type="text"]',
					'input[type="email"]',
					'input[type="number"]',
					'input[type="submit"]',
					'input[type="image"]',
					'label[for]',
					'select',
					'textarea',
					'button',
					'.link',
				]}
			/>
			<Hero />
		</div>
	)
}

export default App
