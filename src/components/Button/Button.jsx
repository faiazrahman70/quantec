import React from 'react'
import './Button.css'

export default function Button() {
	return (
		<>
			<a href="#" onClick={() => console.log('Learn More!')} className="link">
				Learn More
			</a>
		</>
	)
}
