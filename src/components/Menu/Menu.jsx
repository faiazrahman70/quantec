import React, { useState, useEffect } from 'react'
import './Menu.css'

import * as animationData from '../../assets/menu.json'
import Lottie from 'react-lottie'

export default function Menu() {
	const [paused, setPaused] = useState(false)

	const onMouseOver = () => {
		console.log('standing_on_animation')
		setPaused(!paused)
	}

	const onMouseLeave = () => {
		console.log('leaving__')
		setPaused(!paused)
	}

	const defaultOptions = {
		loop: true,
		autoplay: false,
		animationData: animationData,
		rendererSettings: {
			preserveAspectRatio: 'xMidYMid slice',
		},
	}

	return (
		<div className="menu" onMouseLeave={onMouseLeave} onMouseOver={onMouseOver}>
			<Lottie
				options={{ ...defaultOptions }}
				isPaused={!paused}
				isStopped={!paused}
			/>
		</div>
	)
}

//JSX
