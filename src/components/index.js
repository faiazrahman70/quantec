import Video from './VideoBackground.jsx/Video'
import Bar from './Bar/Bar'
import Curtain from './Curtain/Curtain'
import Titles from './Titles/Titles'
import Button from './Button/Button'
import Menu from './Menu/Menu'

export { Video, Button, Bar, Curtain, Menu, Titles }
