import React from 'react'

import './Video.css'

import bgvideo from '../../assets/bgvideo.mp4'

export default function Video({ children }) {
	return (
		<>
			<video src={bgvideo} className="video" autoPlay muted loop></video>
		</>
	)
}
