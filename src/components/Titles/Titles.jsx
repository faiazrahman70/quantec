import React from 'react'
import './Titles.css'

export default function Titles() {
	return (
		<>
			<div className="title_c">
				<p className="title">Coding Solutions,</p>
				<p className="subtitle">Transforming Futures</p>
			</div>

			<div className="slogan_c">
				<p className="slogan">
					Fueling growth through innovative software solutions. We craft
					cutting-edge applications that empower businesses to thrive in the
					digital age, bridging innovation and success.
				</p>
			</div>
		</>
	)
}
