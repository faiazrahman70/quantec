import React, { useEffect, useState } from 'react'
import './Bar.css'

export default function Bar() {
	useEffect(() => {}, [])

	return (
		<div className="bar_c">
			<div className="circle"></div>
			<div className="bar"></div>
		</div>
	)
}
