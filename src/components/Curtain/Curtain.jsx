import React from 'react'
import './Curtain.css'

export default function Curtain() {
	return (
		<>
			<div className="top"></div>
			<div className="bottom"></div>
		</>
	)
}
