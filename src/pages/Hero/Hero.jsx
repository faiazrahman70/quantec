import React from 'react'
import './Hero.css'

import {
	Bar,
	Video as VideoBackground,
	Curtain,
	Titles,
	Button,
	Menu,
} from '../../components'

export default function Hero() {
	return (
		<div className="hero">
			<Curtain />
			<VideoBackground />
			<Menu />
			<Bar />

			<Titles />

			<Button />
		</div>
	)
}
